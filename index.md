---
layout: page
feature_image: "simple-header-blended-small.png"
feature_text: |
  <h1 id="international-workshop-on-artificial-intelligence-in-pathology-aipath-2020" style="color: #ffffff;">International Workshop on Artificial intelligence in Pathology (AIPath 2020)</h1>
  <h6 id="in-conjunction-with" style="color: #ffffff;">in conjunction with</h6>
  <p><a href="http://ieeebibm.org/BIBM2020/" style="text-shadow: none;">2020 IEEE International Conference on Bioinformatics and Biomedicine (IEEE BIBM 2020)</a></p>

---

## Introduction to workshop

The developments of artificial intelligence in pathology have gained its importance in biomedicine. In the past months, accelerations of work in applying state-of-the-art algorithms and providing rapid intelligent model prototyping for digital pathology is visible. Digital slides, as the heart of pathological analyses, are becoming parts of many recent works. However, the potential of the technology is far from being fully exploited in pathology. Except that the accuracy and generality of the algorithms and models are continuously improving, applications also need specialized well-established semantic standards and analysis methods to really meet the criteria of assisting pathologists. 

Therefore, it is important to promote the computational understanding-mechanisms in pathology, which include robust image analysis and more complicated multi-modal methods. The mechanisms should also consist of data management, establishing semantic standards and medically validating the intelligent systems to guarantee the medical usability and enhance the meaningfulness. The works may greatly benefit pathologists and eventually patients, leading to accurate diagnosis and effective treatments. The knowledge from modern intelligence systems and analyses could improve the understanding of human pathology. This workshop is a forum for researchers to discuss new ideas, findings, best practices, and techniques to build up works in pathology, complementing the conventional diagnosis with uses of intelligence in digital pathology. 


## Research topics included in the workshop

1. Data acquisition for computational analysis
2. Data management, e.g. data infrastructure and digital standards/formatting in pathology
3. Image analysis in pathology, e.g. whole-slide image (WSI) and tissue microarray (TMA) analysis
4. Intelligent systems for cancer screening, diagnosis including subtyping, prognosis, and treatment planning
5. Analysis techniques enhancing pathology protocols
6. Medical validation of intelligent systems
7. Application of AI-based systems in clinical cancer practice
8. Semantic resources for pathology, e.g. ontologies and data dictionaries
9. Automated pathology report generation
10. Multimodal data analysis in pathology e.g. integrating radiology scans, electronic medical records, and omics data. 
11. Quantitative histomorphometry and pathology

## Important Dates

- **Oct 15, 2020:** Due date for full workshop papers submission
- November 15, 2020 (<strike>Nov 5, 2020</strike>): Notification of paper acceptance to authors
- Nov. 26, 2020: Camera-ready of accepted papers
- Dec 16-19, 2020: Workshops

## Submission

Please submit a full-length paper (up to 8 page IEEE 2-column format) through the **online submission system** (the link will be available soon). Please follow the [format instruction](http://www.ieee.org/conferences_events/conferences/publishing/templates.html). 

Electronic submissions (in PDF or Postscript format) are required. Selected participants will be asked to submit their revised papers in a format to be specified at the time of acceptance. 

## Program Chairs or co-chairs

- Chen Li – Xi’an Jiaotong University, China (chair)
- Ruifeng Guo – Mayo Clinic, US

## Program Committee Members

- Lixia Yao – Mayo Clinic, US
- Guanjun Zhang – The 1st Affiliated Hospital of Xi’an Jiaotong University, China  
- Hui Guo – The 1st Affiliated Hospital of Xi’an Jiaotong University, China  
- Rui Jiang – Tsinghua University, China  
- Sumitra Thongprasert - Chiang Mai University, Thailand  
- Weiguo Zhu – Peking Union Medical College Hospital, China  
- Xiangrong Zhang – Xidian University, China  
- Xuchao Zhang – Guangdong General Hospital, China  

## Previous workshop
[Artificalintalligence in Pathology 2019](https://bibm2019.aipath.org) in BIBM-2019
