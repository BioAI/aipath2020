---
layout: page
title: Call for Papers
---

## Workshop on Artificial Intelligence in Pathology 2020 (AIPath2020)
in conjunction with the 2020 IEEE International Conference on Bioinformatics and Biomedicine (IEEE BIBM 2020)

Artificial Intelligence in Pathology is a forum for researchers to discuss new ideas, findings, best practices, and techniques to build up works in pathology, advancing digital pathology with information science and technology. 

We welcome submission of research papers (including significant work-in-progress) related to artificial intelligence and pathology. Relevant topics include, but not limited to: 

1. Data acquisition for computational analysis
2. Data management, e.g. data infrastructure and digital standards/formatting in pathology
3. Image analysis in pathology, e.g. whole-slide image (WSI) and tissue microarray (TMA) analysis
4. Intelligent systems for cancer screening, diagnosis including subtyping, prognosis, and treatment planning
5. Analysis techniques enhancing pathology protocols
6. Medical validation of intelligent systems
7. Application of AI-based systems in clinical cancer practice
8. Semantic resources for pathology, e.g. ontologies and data dictionaries
9. Automated pathology report generation
10. Multimodal data analysis in pathology e.g. integrating radiology scans, electronic medical records, and omics data. 
11. Quantitative histomorphometry and pathology

## Paper submission
Please submit a full-length paper (up to 8 page IEEE 2-column format) through the online submission system (you can download the format instruction here https://www.ieee.org/conferences/publishing/templates.html).

Electronic submissions (in PDF or Postscript format) are required. Selected participants will be asked to submit their revised papers in a format to be specified at the time of acceptance. 

## Online Submission:

Link will be available soon. 

## Important dates
- **Oct 15, 2020: Due date for full workshop papers submission**
- Nov 5, 2020: Notification of paper acceptance to authors
- Nov. 26, 2020: Camera-ready of accepted papers
- Dec 16-19, 2020: Workshops

For more information about Artificial Intelligence in Pathology, please visit the workshop website (aipath.org) or contact the organizing team at aipathology@googlegroups.com
